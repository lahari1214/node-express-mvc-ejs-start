/** 
*  Order model
*  Describes the characteristics of each attribute in an order resource.
*
* @author Manasa Goriparthi <s534782@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const transactionSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  categoryID:{},
  transactionID: { type: Number, required: true, unique: true, default: 555 },
  transactionInitiatedDate: { type: Date, required: true, default: Date.now() },
  transactionFinal: { type: Date, required: false },
  paymentType: { type: String, enum: ['NA', 'credit card', 'cash', 'check'], required: true, default: 'NA' },
  paid: { type: Boolean, default: false }
})

module.exports = mongoose.model('transaction', transactionSchema)
