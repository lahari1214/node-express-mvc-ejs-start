/** 
*  Product model
*  Describes the characteristics of each attribute in a product resource.
*
* @author Anusha Kalyankar <S534780@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const CategorySchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  categoryType: { type: String, required: true, unique: false, default: 'category' },
  amountSpent : { type: Number, required: true, default: 0.00, min: 0, max: 100000 }
})

module.exports = mongoose.model('category', CategorySchema)
